/*
 * Assignment01.c
 *
 * Created   : 11/10/2023 15:06
 * Author    : Eoghan Conlon
 * Student ID: 21310262
 * Modified  : 01/11/2023 14:57
 */

/************************************************************************/
/* Header files                                                         */
/************************************************************************/

//Default import
#include <avr/io.h>

//Taken from lab 2
#include <util/delay.h>
#include <avr/cpufunc.h>

//Taken from Lab 3
#include <avr/interrupt.h>

/************************************************************************/
/* Definitions                                                          */
/************************************************************************/

// #defines
//Taken from lab 2
#define NUM_LED_BITS 10

//Taken from lab 3
#define F_CPU 20.0E6

//Taken from lecture 9 slides
#define ZERO_5V 102
#define ONE_0V 205
#define ONE_5V 307
#define TWO_0V 409
#define TWO_5V 512
#define THREE_0V 614
#define THREE_5V 716
#define FOUR_0V 818
#define FOUR_5V 921
#define FIVE_0V 1023

//Own calculations
#define FAST_CYLON 2441
#define SLOW_CYLON 19531

/************************************************************************/
/* Structs                                                              */
/************************************************************************/
/*
* Taken from week 2 lab part 4 submission.
*/
struct LED_BITS {
    PORT_t *LED_PORT;
    uint8_t bit_mapping;
};

struct LED_BITS LED_Array[NUM_LED_BITS] = {
        {&PORTC, 1 << 5},
        {&PORTC, 1 << 4},
        {&PORTA, 1 << 0},
        {&PORTF, 1 << 5},
        {&PORTC, 1 << 6},
        {&PORTB, 1 << 2},
        {&PORTF, 1 << 4},
        {&PORTA, 1 << 1},
        {&PORTA, 1 << 2},
        {&PORTA, 1 << 3}
};



/************************************************************************/
/* Global Variables                                                     */
/************************************************************************/
uint8_t resReady;
uint16_t res;
uint8_t thermometer = 0; //Default to cylon eyes.


/************************************************************************/
/* Functions                                                            */
/************************************************************************/
//Taken from lab 2
void initialiseLED_Port_Bits() {
    for (uint8_t i = 0; i < (NUM_LED_BITS); i += 1) {
        LED_Array[i].LED_PORT->DIRSET = LED_Array[i].bit_mapping;
    }
}

//Taken from lab 3
void CLOCK_init() {
    ccp_write_io((void *) &CLKCTRL.MCLKCTRLB, (0 << CLKCTRL_PEN_bp));
}

void Set_Clear_Ports(uint8_t set) {
    for (uint8_t i = 0; i < (NUM_LED_BITS); i += 1) {
        if (set == 1) {
            LED_Array[i].LED_PORT->OUTSET = LED_Array[i].bit_mapping;
        } else {
            LED_Array[i].LED_PORT->OUTCLR = LED_Array[i].bit_mapping;
        }
    }
}

void InitialiseButton_PORT_bits() {
    PORTE.DIRCLR = PIN1_bm | PIN2_bm;
    PORTE.PIN1CTRL |= PORT_PULLUPEN_bm;
    PORTE.PIN2CTRL |= PORT_PULLUPEN_bm;
}

// Own function
void TCA0_init() {
    TCA0.SINGLE.CTRLA = (7 << 1) | (1 << 0);
    TCA0.SINGLE.PER = 19531; //Default value to 1 second for init
    TCA0.SINGLE.INTCTRL = (1 << 0);
}

void ADC0_init() {
    ADC0.CTRLA = (1 << 1); //Will be enabled later

    //ADC0.CTRLB left at default

    ADC0.CTRLC = (1 << 6) | (1 << 4) | (6 << 0); //Assume Vref > 1V

    ADC0.CTRLD = (1 << 5);

    //ADC0.CTRLE left at default

    //ADC0.SAMPCTRL left at default

    ADC0.MUXPOS = (3 << 0);

    ADC0.INTCTRL = (1 << 0);

    //Starting the ADC
    ADC0.CTRLA |= (1 << 0);
    ADC0.COMMAND = (1 << 0);
}

void RTC_init() {
    RTC.CLKSEL = (1 << 0);
    RTC.PITCTRLA = (13 << 3) | (1 << 0);
    RTC.PITINTCTRL = (1 << 0);
}

void push_init() {
    PORTE.DIRSET = (1 << 1);
}

void Temperature_set(uint8_t numLEDs) {
    Set_Clear_Ports(0);
    for (uint8_t i = 0; i <= numLEDs; i += 1) {
        LED_Array[i].LED_PORT->OUTSET = LED_Array[i].bit_mapping;
    }
}


/************************************************************************/
/* Main Loop                                                            */
/************************************************************************/
int main(void) {
    initialiseLED_Port_Bits();
    CLOCK_init();
    Set_Clear_Ports(0);
    TCA0_init();
    InitialiseButton_PORT_bits();
    ADC0_init();
    RTC_init();
    sei();
    while (1) {
        int8_t LED_output = -1; //Default thermometer to none
        if (resReady) {
            if (res < ZERO_5V) {
                LED_output = -1;
            } else if (res < ONE_0V) {
                LED_output = 0;
            } else if (res < ONE_5V) {
                LED_output = 1;
            } else if (res < TWO_0V) {
                LED_output = 2;
            } else if (res < TWO_5V) {
                LED_output = 3;
            } else if (res < THREE_0V) {
                LED_output = 4;
            } else if (res < THREE_5V) {
                LED_output = 5;
            } else if (res < FOUR_0V) {
                LED_output = 6;
            } else if (res < FOUR_5V) {
                LED_output = 7;
            } else if (res < FIVE_0V) {
                LED_output = 8;
            } else {
                LED_output = 9;
            }
            resReady = 0;
        }
        if (thermometer == 1) {
            if (LED_output == -1) {
                Set_Clear_Ports(0);
            } else {
                Temperature_set(LED_output);
            }
        }
    }
}

/************************************************************************/
/* ISRs                                                                 */
/************************************************************************/

ISR(TCA0_OVF_vect){
        //Static variable declarations
        static uint8_t cycle;
        static uint8_t direction;
        static uint8_t inverted;

        if (thermometer == 0){
            //Checking the push button
            if (PORTE.IN & PIN1_bm) {
                inverted = 1;
                Set_Clear_Ports(0);
                LED_Array[cycle].LED_PORT->OUTSET = LED_Array[cycle].bit_mapping;
            } else {
                inverted = 0;
                Set_Clear_Ports(1);
                LED_Array[cycle].LED_PORT->OUTCLR = LED_Array[cycle].bit_mapping;
            }

            //Implementing the cylon eyes
            if (inverted == 1) {
                if (direction == 0) {
                    if (cycle == 0) {
                        LED_Array[cycle + 1].LED_PORT->OUTCLR = LED_Array[cycle + 1].bit_mapping;
                        LED_Array[cycle].LED_PORT->OUTSET = LED_Array[cycle].bit_mapping;
                    } else {
                        LED_Array[cycle - 1].LED_PORT->OUTCLR = LED_Array[cycle - 1].bit_mapping;
                        LED_Array[cycle].LED_PORT->OUTSET = LED_Array[cycle].bit_mapping;
                    }
                    if (cycle == (NUM_LED_BITS - 1)) {
                        direction = 1;
                    } else {
                        cycle += 1;
                    }
                } else {
                    LED_Array[cycle].LED_PORT->OUTCLR = LED_Array[cycle].bit_mapping;
                    LED_Array[cycle - 1].LED_PORT->OUTSET = LED_Array[cycle - 1].bit_mapping;
                    if (cycle == 1) {
                        direction = 0;
                    } else {
                        cycle -= 1;
                    }
                }
            } else {
                if (direction == 0) {
                    if (cycle == 0) {
                        LED_Array[cycle + 1].LED_PORT->OUTSET = LED_Array[cycle + 1].bit_mapping;
                        LED_Array[cycle].LED_PORT->OUTCLR = LED_Array[cycle].bit_mapping;
                    } else {
                        LED_Array[cycle - 1].LED_PORT->OUTSET = LED_Array[cycle - 1].bit_mapping;
                        LED_Array[cycle].LED_PORT->OUTCLR = LED_Array[cycle].bit_mapping;
                    }
                    if (cycle == (NUM_LED_BITS - 1)) {
                        direction = 1;
                    } else {
                        cycle += 1;
                    }
                } else {
                    LED_Array[cycle].LED_PORT->OUTSET = LED_Array[cycle].bit_mapping;
                    LED_Array[cycle - 1].LED_PORT->OUTCLR = LED_Array[cycle - 1].bit_mapping;
                    if (cycle == 1) {
                        direction = 0;
                    } else {
                        cycle -= 1;
                    }
                }
            }
        }

        //Reset the interrupt flag5
        TCA0.SINGLE.INTFLAGS = 1;
}

ISR(ADC0_RESRDY_vect){
        resReady = 1;
        res = ADC0.RES;
        if (res >= THREE_5V){
            TCA0.SINGLE.PERBUF = FAST_CYLON;
        } else {
            TCA0.SINGLE.PERBUF = SLOW_CYLON;
        }
}

ISR(RTC_PIT_vect){
        if (thermometer == 0){
            thermometer = 1;
        } else {
            Set_Clear_Ports(0);
            thermometer = 0;
        }
        RTC.PITINTFLAGS = (1 << 0);
}

